# Databases

> Personal version of my notes on Databases.

Online version available here: [![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://muskatel.gitlab.io/databases/)

Current build: [![pipeline status](https://gitlab.com/muskatel/databases/badges/main/pipeline.svg)](https://gitlab.com/muskatel/databases/-/commits/main)

## Maintainers

Craig Marais @muskatel

## License

MIT License, see license file.

---

Copyright 2023, Craig Marais (@muskatel)
