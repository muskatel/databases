# Summary

[Introduction](README.md)

- [Installation](01_Installation/README.md)
- [Database Theory](02_Database_Theory/README.md)
- [Creation](03_Creation/README.md)
- [Retrieving Data](04_Retrieving_Data/README.md)
- [Manipulating Data](05_Manipulating_Data/README.md)
- [Merging](06_Merging/README.md)
- [Security and Administration](07_Security_and_Administration/README.md)
- [Normalization](08_Normalization/README.md)
- [Data Modelling](09_Data_Modelling/README.md)
- [Stored Procedures](10_Stored_Procedures/README.md)