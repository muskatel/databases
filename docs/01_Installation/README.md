# Installation

All downloads can be found [here](https://dev.mysql.com/downloads/), but recommended links are providede below.

## Windows

Use the online installed found [here](https://dev.mysql.com/downloads/file/?id=520406).

Select:
- MySQL Server 8.0.34
- MySQL Workbench 8.0.34

The installed should automatically download the required files and dependancies.

## macOS

### MySQL Server

[MySQL Server 8.0.034 - Apple Silicon](https://dev.mysql.com/downloads/file/?id=520328)
[MySQL Server 8.0.034 - x86](https://dev.mysql.com/downloads/file/?id=520329)

### MySQL Workbench

[MySQL Workbench 8.0.034 - Apple Silicon](https://dev.mysql.com/downloads/file/?id=520006)
[MySQL Workbench 8.0.034 - x86](https://dev.mysql.com/downloads/file/?id=520007)

## Docker

Asuming that you have docker and installed, you may use the following to start the required server:

```sh
docker run --name gokstad-mysql-8034 -e MYSQL_ROOT_PASSWORD=hello -p 3306:3306 -d mysql:8.0.34
```

> I use a default password of `hello`, but you should chnage this to suit your preferences.

# Gokstad Sample Databases

For this course we will use example from:
- northwind
- classicmodels
- world

A sample file for imprting these can be found here: [gokstad-all.sql](https://gitlab.com/muskatel/databases/-/blob/main/resources/gokstad-all.sql).

---

Copyright 2023, Craig Marais (@muskatel)